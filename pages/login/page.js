import { Selector } from 'testcafe';

export default class Page {
  constructor() {
    const container = Selector('#login');

    this.email = Selector('#email');
    this.senha = Selector('#senha');
    this.boxMsg = container.find('.alert-danger');
  }
}
