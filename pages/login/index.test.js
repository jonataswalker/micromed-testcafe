import { SITE, LOGIN } from '../../konstants';
import Page from './page';

const page = new Page();

fixture`Login`.page(SITE);

test('Logando incorretamente', async t => {
  await t
    .typeText(page.email, LOGIN.USER)
    .typeText(page.senha, LOGIN.WRONG_PWD)
    .pressKey('enter')
    .expect(page.boxMsg.innerText)
    .eql('Email ou senha incorretos!')
    .typeText(page.email, LOGIN.WRONG_USER)
    .typeText(page.senha, LOGIN.PWD)
    .expect(page.boxMsg.innerText)
    .eql('Email ou senha incorretos!');
});
